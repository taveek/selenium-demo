from robot.libraries.BuiltIn import BuiltIn

def output_video_url():
    session_id = BuiltIn().get_library_instance('Selenium2Library')._current_browser().session_id
    BuiltIn().log_to_console("VIDEO_URL=https://s3-us-west-1.amazonaws.com/........./play.html?"+session_id)

